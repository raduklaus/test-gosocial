<?php
$persons = [
  "Tom" => ["father" => "Jerry", "mother" => "Ariel"],
  "Jerry" => ["father" => "Simon", "mother" => "Ruth"],
  "Simon" => ["father" => "Leo"],

];



function getOldestParent($persons, $name, $generationNumber = 0) {
  if (!$persons[$name]) {
      return $name . '-' . $generationNumber;
  }


  $fatherAncestor = getOldestParent($persons, $persons[$name]["father"], $generationNumber + 1);

  if (!$persons[$name]["mother"]) {
      return $fatherAncestor;

  } else {
      $motherAncestor = getOldestParent($persons, $persons[$name]["mother"], $generationNumber + 1);

      list($fatherName, $fatherGenerationNo) = explode("-", $fatherAncestor);
      list($motherName, $motherGenerationNo) = explode("-", $motherAncestor);


      if ($fatherGenerationNo > $motherGenerationNo) {
              return $fatherAncestor;
      } else {
              return $motherAncestor;
      }
  }

}

$oldestParent = '';
$oldestGeneration = 0;

foreach ($persons as $name => $parents) {
  list($ancestorName, $generation) = explode("-", getOldestParent($persons, $name));

  if ($generation > $oldestGeneration) {
    $oldestParent = $ancestorName;
    $oldestGeneration = $generation;
  }
}

echo 'The oldest parent is: <strong>' .  $oldestParent .  '</strong> and the number of generations are <strong>' . $oldestGeneration . '</strong>';